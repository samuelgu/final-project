Customer Information
Customer Name: Hans Dayson
Credit Score: 0.98

Transactions
Loan Type: Student Loan
Amount Borrowed: 500
Interest Rate: 0.41666666666666663
Amount Paid: 521
Amount Owed: 479

Loan Type: Car Loan
Amount Borrowed: 9000
Interest Rate: 0.28800000000000003
Amount Paid: 550
Amount Owed: 10450

MTN Mobile Money Information
Mobile Money PhoneNumber: 0245673892
Total Deposits: 620
Total Withdrawals: 500
Balance: 50
