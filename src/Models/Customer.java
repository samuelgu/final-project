/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author User
 */
public class Customer{
        public String customerID;
        public String surname;
	public String firstName;
	public double creditScore;
	public double credit;
	public String userName;
        public String passWord;
        /**
         * @param id Customer ID
         * @param su Surname
         * @param f First name
         * @param ad credit score
         * @param g credit
         * @param uu username
         * @param pp password
         */
        public Customer(String id, String su, String f, double ad, double g, String uu, String pp)
        {
            customerID = id;
            surname = su;
            firstName = f;
            creditScore = ad;
            credit = g;
            userName = uu;
            passWord = pp;
        }
        public Customer()
        {
            customerID = null;
            surname = null;
            firstName = null;
            creditScore = 0;
            credit = 0;
            userName = null;
            passWord = null;
        }
}