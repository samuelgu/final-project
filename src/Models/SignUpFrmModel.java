/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Mac-Noble
 */
public class SignUpFrmModel {
    public boolean add(Customer s)
    {
        boolean success = false;
        try
        { 
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/credifi?user=root&password=grace@50");
            Statement stmt = conn.createStatement();
            ResultSet rs  = stmt.executeQuery("SELECT * FROM `mobile_money_data`");
            PreparedStatement p = conn.prepareStatement("Insert Into customer (customerID, `customer_name`, `credit_score`, credit, userName, password) VALUES "
                    + "(?,?,?,?,?,?)");
            p.setString(1, s.customerID);
            p.setString(2, s.firstName + " " + s.surname);
            //generate credit score
            s.creditScore = generateCreditScore(s, rs);
            p.setDouble(3, s.creditScore);
            p.setDouble(4, s.credit);
            p.setString(5, s.userName);
            p.setString(6, s.passWord);
            p.execute();
            success = true;
            p.close();
            conn.close();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public double generateCreditScore(Customer c, ResultSet rs)
    {
        double credit_score = 1;
        try{
         while (rs.next()) {
                String _cID = rs.getString("phoneNumber");
                if(_cID.equals(c.customerID))
                {
                    double bal = rs.getDouble("balance");
                    if (bal <= 1000)
                        credit_score -= 0.2;
                    else if(bal <= 2000)
                        credit_score -= 0.3;
                    else if(bal <= 3000)
                        credit_score -= 0.4;
                    else
                        credit_score -= 0.5;
                    double wid = rs.getDouble("withdrawals");
                    double dep = rs.getDouble("deposits");
                    int widFreq = rs.getInt("withdrawalFreq");
                    int depFreq = rs.getInt("depositFreq");
                    double wdDiff = wid/dep;
                    double wdFreqDiff = (double) widFreq/depFreq;
                    if (wdDiff <= 0.5)
                        credit_score -= 0.2;
                    else if(wdDiff <= 1)
                        credit_score -= 0.1;
                    else
                        credit_score -= 0.05;
                    if (wdFreqDiff <= 0.5)
                        credit_score -= 0.2;
                    else if(wdFreqDiff <= 1)
                        credit_score -= 0.1;
                    else
                        credit_score -= 0.05;
                }
            }
        }catch(Exception e){}
        
        return credit_score;
    }
    public boolean checkNumber(String id)
    {
        boolean success = false;
        try{
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/credifi?user=root&password=grace@50");
            Statement stmt = conn.createStatement();
            ResultSet rs  = stmt.executeQuery("SELECT * FROM `mobile_money_data`");
            while (rs.next()) {
                   String _cID = rs.getString("phoneNumber");
                   if(_cID.equalsIgnoreCase(id))
                   {
                       success = true;
                       break;
                   }
            }
            rs.close();
            conn.close();
         }catch(Exception e){};
        return success;
    }
}
