/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author fad51
 */
public class DeleteModel {
    /*
    *@param id This is customer's Id
    */
    public void delete(String id){
            try{
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/credifi?user=root&password=grace@50");
            PreparedStatement p = conn.prepareStatement("Delete from customer where customerID = ?");
                 p.setString(1,id);
                 p.executeUpdate();
            p.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }    
    /*
    *This method checks whether a customer is owing
    *@param id Customer Id
    */
    public boolean isOwing(String id)
    {
        boolean owing = false;
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
            "jdbc:mysql://localhost/credifi?user=root&password=grace@50");
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("SELECT * FROM transactions WHERE phone_number = '"+id+"'");
            while(rs.next()) {
                double am = rs.getDouble("amount_owed");
                if(am > 0)
                {
                    owing = true;
                    break;
                }
            }
            rs.close();
            conn.close();
        }
        catch (Exception e) {
                e.printStackTrace();
        }
        return owing;
    }
}

