/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Mac-Noble
 */
public class EditFrmModel {
    /*
    *This method performs the edit
    *@param id Customer id
    *@param username Username
    *@param passWord User password
    *@return success If edit was successful
    */
    public boolean edit(String id, String username, String passWord)
    {
        boolean success = false;
        try
        { 
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/credifi?user=root&password=grace@50");
            Statement stmt = conn.createStatement();
            PreparedStatement p = conn.prepareStatement("Update customer set userName = ?, password = ? WHERE customerID = ?");
            p.setString(1, username);
            p.setString(2, passWord);
            p.setString(3, id);
            p.executeUpdate();
            success = true;
            p.close();
            conn.close();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
}
