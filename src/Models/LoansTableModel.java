/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Mac-Noble
 */
public class LoansTableModel extends AbstractTableModel {
    private final ArrayList<Loan> arr;
    static LoansTableModel theModel = null;
    String[] columnNames = null;
    
    

    public LoansTableModel() {
        super();     
        arr = new ArrayList<Loan>();
        fetchTableData(); //my own function to initialize the araylist
    }
    
    public static LoansTableModel getInstance(){     //I am using the Singleton pattern here
        if (theModel == null)
            theModel = new LoansTableModel();
        return theModel;
    }
    
    @Override
    public int getColumnCount() {        
        return columnNames.length; //we know student has 4 fields
    }

    @Override
    public int getRowCount() {
        return arr.size();
    }

    @Override
    public Object getValueAt(int row, int column) {
       Loan loan = arr.get(row);
        if (column == 0)
            return loan.getType();
        else if (column == 1)
            return loan.getMinimum();
        else if (column == 2)
            return loan.getMaximum();
         else if (column == 3)
            return loan.getBaseInterest();
        else
            return null; 
    }//fxn getValueAt
    
    
    //optional functions
  
    @Override
    public String getColumnName(int col) {
        return columnNames[col];//otherwise returns A, B, C etc
    }
    //overide this to allow editing of some columns/rows

    @Override
    public boolean isCellEditable(int row, int col) {
	return false;
    }
        
    // uses this to determine the default renderer or 
    // editor for each cell. 
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }
    
    void fetchTableData(){
    try {
        Connection conn = null;
	Class.forName("com.mysql.jdbc.Driver").newInstance();
	conn = java.sql.DriverManager.getConnection(
	"jdbc:mysql://localhost/credifi?user=root&password=grace@50");
	
        Statement s = conn.createStatement();
        
        ResultSet  rs = s.executeQuery("SELECT * FROM loans");
        ResultSetMetaData meta = rs.getMetaData();
        int numberOfColumns = meta.getColumnCount();
        columnNames = new String[numberOfColumns];
        for (int i = 1; i <= numberOfColumns; i++) { //note its 1 based.
            columnNames[i - 1] = meta.getColumnName(i); //columns Zero based.
        }
        
        ArrayList <Object> rows = new ArrayList<Object>();
        //get actual row data
        
        while(rs.next()) {
            Loan st = new Loan(rs.getString(1), rs.getDouble(2), rs.getDouble(3),rs.getDouble(4));
            arr.add(st);
        }
    }
    catch (Exception e) {
            e.printStackTrace();
    }	

        
    }
    
}
