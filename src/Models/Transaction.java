/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author User
 */
public class Transaction{
        private String transactionsID;
        private String loanType;
        private String phone_number;
        private double principal;
        private double interest;
	private double amountOwed;
        private double amountPaid;
        
        public Transaction(String id,String ph, String lt,  double p, double ii, double ap, double ao)
        {
            transactionsID = id;
            loanType = lt;
            principal = p;
            amountOwed = ao;
            amountPaid = ap;
            interest = ii;
            phone_number = ph;
        }

    public double getAmountOwed() {
        return amountOwed;
    }

    public double getAmountPaid() {
        return amountPaid;
    }

    public String getLoanType() {
        return loanType;
    }

    public double getPrincipal() {
        return principal;
    }

    public String getTransactionsID() {
        return transactionsID;
    }

    public double getInterest() {
        return interest;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setAmountOwed(double amountOwed) {
        this.amountOwed = amountOwed;
    }

    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }
    
}