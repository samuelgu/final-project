/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Mac-Noble
 */
public class PrintModel {
    public void printHistory(String id)
    {
        final JFileChooser fc = new JFileChooser();
        int result = fc.showSaveDialog(null);
        
        if(result == JFileChooser.APPROVE_OPTION)
        {
            try(PrintWriter pr = new PrintWriter(new FileWriter(fc.getSelectedFile()))){ 
                Connection conn = null;
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/credifi?user=root&password=grace@50");
                Statement stmt = conn.createStatement();

                ResultSet rs1  = stmt.executeQuery("SELECT * FROM `customer` WHERE customerID = '"+id+"'");
                pr.println("Customer Information");
                while(rs1.next())
                {
                    pr.println("Customer Name: " + rs1.getString("customer_name"));
                    pr.println("Credit Score: " + rs1.getString("credit_score"));
                    break;
                }
                pr.println();
                pr.println("Transactions");
                ResultSet rs  = stmt.executeQuery("SELECT * FROM `transactions` WHERE phone_number = '"+id+"'");
                while(rs.next())
                {
                    pr.println("Loan Type: "+rs.getString("loan_type"));
                    pr.println("Amount Borrowed: "+rs.getString("principal"));
                    pr.println("Interest Rate: "+rs.getString("interest"));
                    pr.println("Amount Paid: "+rs.getString("amount_paid"));
                    pr.println("Amount Owed: "+rs.getString("amount_owed"));
                    pr.println();
                }
                ResultSet rs2  = stmt.executeQuery("SELECT * FROM `mobile_money_data` WHERE phoneNumber = '"+id+"'");
                pr.println("MTN Mobile Money Information");
                while(rs2.next())
                {
                    pr.println("Mobile Money PhoneNumber: "+rs2.getString("phoneNumber"));
                    pr.println("Total Deposits: "+rs2.getString("deposits"));
                    pr.println("Total Withdrawals: "+rs2.getString("withdrawals"));
                    pr.println("Balance: "+rs2.getString("balance"));
                    break;
                }
                System.out.println("Done Printing");
                rs.close();
                rs1.close();
                rs2.close();
                conn.close();
            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        else
            JOptionPane.showMessageDialog(null, "You did not select any any file. Please try again!");
    } 
}
