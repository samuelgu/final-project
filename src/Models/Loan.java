/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author User
 */
public class Loan{
        private String type;
        private double minimum;
	private double maximum;
	private double baseInterest;
        /*
        *This constructs the loan object
        *@param ty Loan type
        *@param mm Maximum amount that can be borrowed
        *@param mn Minimum amount that can be borrowed
        @param b_interest Base interest for the loan
        */
        public Loan(String ty, double mn, double mm, double b_interest)
        {
            type = ty;
            minimum = mn;
            maximum = mm;
            baseInterest = b_interest;
        }
    /*
        *@return baseInterest the base interest of the loan
        */    
    public double getBaseInterest() {
        return baseInterest;
    }
     /*
        *@return maximum returns maximum amount
        */    
    public double getMaximum() {
        return maximum;
    }
     /*
        *@return minimum returns minimum amount
        */    
    public double getMinimum() {
        return minimum;
    }
     /*
        *@return type loan type
        */    
    public String getType() {
        return type;
    }    
}