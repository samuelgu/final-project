/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Mac-Noble
 */
public class TransactionsTableModel extends AbstractTableModel {
    private final ArrayList<Transaction> arr;
    private final ArrayList<Boolean> comp;
    static TransactionsTableModel theModel = null;
    String[] columnNames = null;
    
    static String cID;

    public TransactionsTableModel(String id) {
        super();   
        cID = id;
        comp = new ArrayList<Boolean>();
        arr = new ArrayList<Transaction>();
        fetchTableData(id); //my own function to initialize the araylist
    }
    
    public static TransactionsTableModel getInstance(){     //I am using the Singleton pattern here
        if (theModel == null)
            theModel = new TransactionsTableModel(cID);
        return theModel;
    }
    
    @Override
    public int getColumnCount() {        
        return columnNames.length; //we know student has 4 fields
    }

    @Override
    public int getRowCount() {
        return arr.size();
    }

    @Override
    public Object getValueAt(int row, int column) {
       Transaction trans = arr.get(row);
        if (column == 0)
            return trans.getTransactionsID();
        else if (column == 1)
            return trans.getPhone_number();
        else if (column == 2)
            return trans.getLoanType();
         else if (column == 3)
            return trans.getPrincipal();
         else if (column == 4)
            return trans.getInterest();
         else if (column == 5)
            return trans.getAmountPaid();
         else if (column == 6)
            return trans.getAmountOwed();  
         else if (column == 7)
            return comp.get(row);
        else
            return null; 
    }//fxn getValueAt
    
    
    //optional functions
  
    @Override
    public String getColumnName(int col) {
        return columnNames[col];//otherwise returns A, B, C etc
    }
    //overide this to allow editing of some columns/rows

    @Override
    public boolean isCellEditable(int row, int col) {
        if(col == 7)
            return true;
        else
            return false;
    }
    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {
        if(7 == columnIndex)
        {
            for(int i=0; i < comp.size(); i++)
                if(i != rowIndex)
                {
                    comp.set(i, false);
                    fireTableCellUpdated(i, columnIndex);
                }
                else
                    comp.set(rowIndex, true);
        }
        fireTableCellUpdated(rowIndex, columnIndex);
    }
        
    // uses this to determine the default renderer or 
    // editor for each cell. 
    @Override
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }
    
    void fetchTableData(String id){
    try {
        Connection conn = null;
	Class.forName("com.mysql.jdbc.Driver").newInstance();
	conn = java.sql.DriverManager.getConnection(
	"jdbc:mysql://localhost/credifi?user=root&password=grace@50");
	
        Statement s = conn.createStatement();
        
        ResultSet rs = s.executeQuery("SELECT * FROM transactions where phone_number = '"+id+"'");
        ResultSetMetaData meta = rs.getMetaData();
        int numberOfColumns = meta.getColumnCount();
        columnNames = new String[numberOfColumns + 1];
        for (int i = 1; i <= numberOfColumns; i++) { //note its 1 based.
            columnNames[i - 1] = meta.getColumnName(i); //columns Zero based.
        }
        columnNames[numberOfColumns]="Check-to-Pay"; 
        ArrayList <Object> rows = new ArrayList<Object>();
        //get actual row data
        
        while(rs.next()) {
            Transaction st = new Transaction(rs.getString("transactionID"), rs.getString("phone_number"),rs.getString("loan_type")
                    , rs.getDouble("principal"),rs.getDouble("interest"),rs.getDouble("amount_paid"),rs.getDouble("amount_owed"));
            arr.add(st);
            comp.add(false);
        }
        rs.close();
        conn.close();
        System.out.println("Done fecthing data");
    }
    catch (Exception e) {
            e.printStackTrace();
    }	

        
    }

    public ArrayList<Transaction> getArr() {
        return arr;
    }
    public boolean makePayment(int row, double amount)
    {
        boolean success = false;
        try
        { 
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/credifi?user=root&password=grace@50");
            Statement stmt = conn.createStatement();
            PreparedStatement p = conn.prepareStatement("Update transactions set amount_paid = ?, amount_owed = ? WHERE transactionID = ?");
            double amount_paid = arr.get(row).getAmountPaid()+ amount;
            p.setDouble(1, amount_paid);
            double amount_owed = arr.get(row).getAmountOwed() - amount;
            p.setDouble(2, amount_owed);
            p.setString(3, arr.get(row).getTransactionsID());
            p.executeUpdate();
            arr.get(row).setAmountOwed(amount_owed);
            arr.get(row).setAmountPaid(amount_paid);
            fireTableDataChanged();
            success = true;
            p.close();
            conn.close();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }

    public boolean hasEnoughBalance(String am, String id) {
        boolean success = false;
        try
        { 
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/credifi?user=root&password=grace@50");
            Statement stmt = conn.createStatement();
            ResultSet rs  = stmt.executeQuery("SELECT * FROM `mobile_money_data` WHERE phoneNumber = '"+id+"'");
            double amount = Double.parseDouble(am);
            while(rs.next())
            {
                if(rs.getDouble("balance") >= amount)
                { 
                    double diff = rs.getDouble("balance")-amount;
                    success = true;
                    PreparedStatement p = conn.prepareStatement("Update mobile_money_data set balance = ? WHERE phoneNumber = ?");
                    p.setDouble(1, diff);
                    p.setString(2, id);
                    p.executeUpdate();
                    break;
                }
            }

            rs.close();
            conn.close();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
}
