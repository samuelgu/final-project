/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SamuelGU
 */
public class LoanFrmModel {
    public void fillLoans(ArrayList<Loan> arr)
    {
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
            "jdbc:mysql://localhost/credifi?user=root&password=grace@50");
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("SELECT * FROM loans");
            while(rs.next()) {
                Loan st = new Loan(rs.getString(1), rs.getDouble(2), rs.getDouble(3),rs.getDouble(4));
                arr.add(st);
            }
            rs.close();
            conn.close();
        }
        catch (Exception e) {
                e.printStackTrace();
        }	
    }
    public boolean add(String lType, double amount, String ph, double interest)
    {
        boolean success = false;
        try
        { 
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/credifi?user=root&password=grace@50");
            Statement stmt = conn.createStatement();
            PreparedStatement p = conn.prepareStatement("Insert Into transactions (transactionID, `phone_number`, `loan_type`,"
                    + " principal, interest, amount_paid, amount_owed) "
                    + "VALUES (?,?,?,?,?,?,?)");
            Calendar cl = Calendar.getInstance();
            p.setString(1, cl.getTime().toString());
            p.setString(2, ph);
            p.setString(3, lType);
            p.setDouble(4, amount);
            p.setDouble(5, interest);
            p.setDouble(6, 0);
            p.setDouble(7, amount*(1+interest));
            p.executeUpdate();
            success = true;
            p.close();
            conn.close();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
     public double getCreditScore(String id)
    {
        double credit_Score = 1;
        try {
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection(
            "jdbc:mysql://localhost/credifi?user=root&password=grace@50");
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("SELECT * FROM customer WHERE customerID = '"+id+"'");
            while(rs.next()) {
                credit_Score = rs.getDouble("credit_score");
            }
        }
        catch (Exception e) {
                e.printStackTrace();
        }
        return credit_Score;
    }
}
