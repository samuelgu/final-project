/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Mac-Noble
 */
public class LoginFrmModel {
    private String name;
    private String id;
    private boolean success = false;
    public boolean logIn(String us, String ps)
    {
        try
        { 
            Connection conn = null;
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = java.sql.DriverManager.getConnection("jdbc:mysql://localhost/credifi?user=root&password=grace@50");
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("SELECT * FROM customer");
            
            while (rs.next()) {
                String _u = rs.getString("userName");
                String _p = rs.getString("password");
                name = rs.getString("customer_name");
                if(_u.equals(us) && _p.equals(ps))
                {
                    success = true;
                    id = rs.getString("customerID");
                    break;
                }
            }
            rs.close();
            conn.close();       
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public String getCustomerName()
    {
        return name;
    }
    public String getCustomerID()
    {
        return id;
    }
    public boolean isLoggedIn()
    {
        return success;
    }
}
