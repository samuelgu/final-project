/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Customer;
import Models.Loan;
import Models.LoanFrmModel;
import Views.LoansFrm;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Mac-Noble
 */
public class LoanFrmController implements ActionListener {
    LoansFrm view;
    LoanFrmModel model;
    ArrayList<Loan> loans;
    String customerID;
    public LoanFrmController(LoansFrm r, LoanFrmModel m, String id) {
        view = r;
        view.getContentPane().setBackground(new Color(14,52,65));
        loans = new ArrayList<Loan>(); 
        customerID = id;
        model = m;
        model.fillLoans(loans);
        view.setVisible(true);
    }
    public void actionPerformed(ActionEvent ae) {            
//        if (ae.getSource() == view.getApplyLoanBtn())
//        {      
//            view.setVisible(false);
//            view.dispose();
//        }
        if (ae.getSource() == view.getApplyLoanBtn())
        {               
           String amount = view.getAmount().getText();
           String loanType = (String) view.getLoanType().getSelectedItem();
           if(isNumeric(amount))
           {
                for(Loan loan : loans)
                {
                    if(loan.getType().equalsIgnoreCase(loanType))
                    {
                        double am = Double.parseDouble(amount);
                        double mn = loan.getMinimum(), mm = loan.getMaximum();
                        if(am >= mn && am <= mm)
                        {
                            double interest = loan.getBaseInterest() + model.getCreditScore(customerID)/10;
                            double percent = interest * 100;
                            int response = JOptionPane.showConfirmDialog(null, "The interest for you loan is\n"+ percent+"%");
                            if(response == JOptionPane.OK_OPTION)
                            {
                                System.out.println("Making Loan: "+ customerID);
                                if(model.add(loanType, am, customerID, interest))
                                        JOptionPane.showMessageDialog(null, "You have successfully applied for a loan.\n"
                                                + "You should receive the amount in total in your MTN Mobile Money account");
                                view.dispose();   
                            }
                            else
                                JOptionPane.showMessageDialog(null, "You can apply for anothe type of loan");
                        }
                    }
                }
           }
           else
               JOptionPane.showMessageDialog(null, "Please, enter amount in digits");
        }
    }
    public void control()
    {
       // view.getCancelBtn().addActionListener(this);
        view.getApplyLoanBtn().addActionListener(this);
    }
    public boolean isNumeric(String s)
    {
        return s.matches("[-+]?\\d*\\.?\\d+");
    }
}
