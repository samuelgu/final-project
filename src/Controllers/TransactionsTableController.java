/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.SearchModel;
import Models.TransactionsTableModel;
import Views.DisplaySearch;
import Views.MainPageView;
import Views.TransactionsTable;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Mac-Noble
 */
public class TransactionsTableController implements ActionListener {
    TransactionsTable view;
    TransactionsTableModel model;
    MainPageView main;
    public TransactionsTableController(TransactionsTable r, TransactionsTableModel m, MainPageView v) {
        model = m;
        view = r;
        main = v;
        view.getTable().setBackground(new Color(0,102,102));
        view.setLocationRelativeTo(main);
        view.getTable().setModel(model);
        view.setVisible(true);
    }    
    public void actionPerformed(ActionEvent ae) {            
        if (ae.getSource() == view.getExitButton() )
        {               
            view.dispose();
        }
        if (ae.getSource() == view.getSearchButton() )
        {               
            String response = JOptionPane.showInputDialog("Enter the loan type to search you want to pay");
            new SearchModel().search(response, new DisplaySearch());
        }
        if (ae.getSource() == view.getRepayButton() )
        {               
            int row = view.getTable().getSelectedRow();
            String response = JOptionPane.showInputDialog("Enter the amount you want to pay");
            if(response != null)
            {
                if(isNumeric(response))
                {
                    if(model.hasEnoughBalance(response, main.customerId))
                    {
                        if(model.makePayment(row, Double.parseDouble(response)))
                        {
                            JOptionPane.showMessageDialog(null, "You have successfully made your loan repayment!");
                        }
                    }
                    else
                        JOptionPane.showMessageDialog(null, "Sorry there...you don't have enough balance in your\n"
                                + "MTN Mobile Money account.");    
                }else
                {
                    JOptionPane.showMessageDialog(null, "Please, amount must be numbers!");
                    view.getRepayButton().doClick();
                }
            }    
        }
    }
     public void control(){
        view.getExitButton().addActionListener(this);
        view.getSearchButton().addActionListener(this);
        view.getRepayButton().addActionListener(this);
     }
      private static boolean isNumeric(String s)
    {
      return s.matches("[-+]?\\d*\\.?\\d+") && !s.contains("-"); 
    }
}
