/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Customer;
import Models.SignUpFrmModel;
import Views.MainPageView;
import Views.SignUpFrm;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author Mac-Noble
 */
public class SignUpFormController implements ActionListener {
    SignUpFrm view; 
    SignUpFrmModel model;
    //I have not been consistent in view & model naming. 
    //should be addNewForm etc.
    public SignUpFormController(SignUpFrm v, SignUpFrmModel m, MainPageView main) {
        view = v;
        view.getContentPane().setBackground(new Color(14,52,65));
        model = m;
        view.getAddButton().addActionListener(this);
        view.getCancelButton().addActionListener(this);
        view.setLocationRelativeTo(main);
        view.setVisible(true);     
    }
    @Override
    public void actionPerformed(ActionEvent ae) {            
        if (ae.getSource() == view.getCancelButton() )
        {
            view.dispose();
        }
        if (ae.getSource() == view.getAddButton())
        {  
            String id, su,f,u,p;
            double ad, g;
                id = view.getPhoneNumber().getText();
                su = view.getSurname().getText();
                f = view.getFirstName().getText();
                ad = 0.0;
                g = 0.0;
                u = view.getUser().getText();
                p = new String(view.getPassWord().getPassword());
                if(!isEmpty(id,su,f,u,p))
                {
                    if(isMTNMobileMoneyNumber(id))
                    {
                         Customer s = new Customer(id, su, f, ad, g, u, p);
                        if (model.add(s))
                        {
                            JOptionPane.showMessageDialog(null, "You have succesfully created an account!\n"
                                    + "You will receive an SMS message to activate your account!\nEnjoy crediFi!");
                            view.dispose();
                        }
                    }
                    else
                        JOptionPane.showMessageDialog(null, "Please! Enter your MTN mobile money number");
                } else
                        JOptionPane.showMessageDialog(null, "Make sure each field is entered correctly.");
           
        }
    }
     public void control(){
        view.getAddButton().addActionListener(this);
        view.getCancelButton().addActionListener(this);
    }
     private static boolean isTelephoneNumber(String s)
    {
      return s.matches("[-+]?\\d*\\.?\\d+") && s.length() == 10; 
    }
     public boolean isMTNMobileMoneyNumber(String id)
     {
         return isTelephoneNumber(id) && (id.startsWith("024") || id.startsWith("054") && model.checkNumber(id));
     }
     public boolean isEmpty(String s, String st, String u, String p, String q)
     {
       return s.trim().equals("") || st.trim().equals("")|| u.trim().equals("")|| p.trim().equals("")|| q.trim().equals("");
     }
}
