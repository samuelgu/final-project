/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Customer;
import Models.LoanFrmModel;
import Models.LoansTableModel;
import Views.LoansFrm;
import Views.LoansTable;
import Views.MainPageView;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Mac-Noble
 */
public class LoansTableController implements ActionListener {
    LoansTable view;
    LoansTableModel model;
    MainPageView main;
    public LoansTableController(LoansTable r, LoansTableModel m, MainPageView v) {
        model = m;
        view = r;
        //view.getContentPane().setBackground(new Color(14,52,65));
        main = v;
        view.getTable().setBackground(new Color(0,102,102));
        view.getTable().setModel(model);
        view.setVisible(true);
    }    
    public void actionPerformed(ActionEvent ae) {            
        if (ae.getSource() == view.getExitButton() )
        {               
            view.dispose();
        }
        if (ae.getSource() == view.getLoanButton() )
        {               
            new LoanFrmController(new LoansFrm(), new LoanFrmModel(), main.customerId).control();
        }
    }
     public void control(){
        view.getExitButton().addActionListener(this);
        view.getLoanButton().addActionListener(this);
     }
}
