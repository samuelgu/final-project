/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Customer;
import Models.EditFrmModel;
import Models.SignUpFrmModel;
import Views.EditFrm;
import Views.MainPageView;
import Views.SignUpFrm;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author Mac-Noble
 */
public class EditFrmController implements ActionListener {
    EditFrm view; 
    EditFrmModel model;
    MainPageView main;
    //I have not been consistent in view & model naming. 
    //should be addNewForm etc.
    public EditFrmController(EditFrm v, EditFrmModel m, MainPageView mn) {
        view = v;
        view.getContentPane().setBackground(new Color(14,52,65));
        model = m;
        main = mn;
        view.getEditButton().addActionListener(this);
        view.getCancelButton().addActionListener(this);
        view.setLocationRelativeTo(main);
        view.setVisible(true);     
    }
    @Override
    public void actionPerformed(ActionEvent ae) {            
        if (ae.getSource() == view.getCancelButton() )
        {
            view.dispose();
        }
        if (ae.getSource() == view.getEditButton())
        {  
                String u = view.getUser().getText();
                String p = new String(view.getPassWord().getPassword());
                if(!isEmpty(u,p))
                {
                        if (model.edit(main.customerId,u,p))
                        {
                            JOptionPane.showMessageDialog(null,"You have succesfully edited your account!");
                            view.dispose();
                        }
                } else
                        JOptionPane.showMessageDialog(null, "Make sure each field is entered correctly.");
        }
           
    }
     public void control(){
        view.getEditButton().addActionListener(this);
        view.getCancelButton().addActionListener(this);
    }
     public boolean isEmpty(String s, String st)
     {
       return s.trim().equals("") || st.trim().equals("");
     }
}
