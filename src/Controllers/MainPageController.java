/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Customer;
import Models.DeleteModel;
import Models.EditFrmModel;
import Models.LoansTableModel;
import Models.LoginFrmModel;
import Models.PrintModel;
import Models.SignUpFrmModel;
import Models.TransactionsTableModel;
import Views.EditFrm;
import Views.LoansTable;
import Views.LoginFrm;
import Views.LoginView;
import Views.MainPageView;
import Views.SignUpFrm;
import Views.TransactionsTable;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Mac-Noble
 */
public class MainPageController implements ActionListener {
    MainPageView view;
    public MainPageController(MainPageView r) {
        view = r;
        view.getContentPane().setBackground(new Color(14,52,65));
        view.getCustomer().setVisible(false);
        view.getServices().setVisible(false);
        view.setLocationByPlatform(true);
        view.setVisible(true);
    }
    public void actionPerformed(ActionEvent ae) {            
        if (ae.getSource() == view.getExitMenu())
        {               
            view.dispose();
        }
        if (ae.getSource() == view.getInfoBtn())
        {  
            System.out.println(view.customerId);
            StringBuilder str = new StringBuilder();
            try(BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\Mac-Noble\\Documents\\NetBeansProjects\\Final_Project\\src\\Files\\about.txt")))
            {
                String x;
                while((x = br.readLine()) != null)
                {
                    str.append(x);
                }
            }catch(Exception e){e.printStackTrace();}
            String aboutUS = "CrediFi is a platform that allows MTN Mobile Money users to access\n"
                    + "short-term loans payable in one year.\n"
                    + "To use this platform, simply create an account with your MTN Mobile"
                    + "Money Number.\nYou will receive a confirmation message from MTN to"
                    + "activate your crediFi account. \nOnce done, you can access our loan services"
                    + "at affordable interest rates!";
            //JOptionPane.showMessageDialog(null, str.toString());
            JOptionPane.showMessageDialog(null, aboutUS, "About Us!", JOptionPane.INFORMATION_MESSAGE);
        }
        if (ae.getSource() == view.getSignInBtn())
        {  
            new SignUpFormController(new SignUpFrm(), new SignUpFrmModel(), view).control();
        }
        if (ae.getSource() == view.getLoginBtn())
        { 
            new LogInController(new LoginView(), new LoginFrmModel(), view);
        }
        if (ae.getSource() == view.getBackMenu())
        { 
            view.dispose();
            new MainPageController(new MainPageView()).control();
        }
        if (ae.getSource() == view.getLoanS())
        { 
            new LoansTableController(new LoansTable(), new LoansTableModel(), view).control();
        }
        if (ae.getSource() == view.getDeleteMenu())
        { 
            DeleteModel model = new DeleteModel();
            if(!model.isOwing(view.customerId))
            {
                int response = JOptionPane.showConfirmDialog(null, "Do you wish to go ahead with the delete");
                if(response == JOptionPane.OK_OPTION)
                {
                    model.delete(view.customerId);
                    view.getBackMenu().doClick();
                }
            }
            else
                JOptionPane.showMessageDialog(null, "Please, you still have unpaid debts!\nYou can only "
                        + "delete your account when you have paid us our money! Have a good day.");
        }
        if (ae.getSource() == view.getEditMenu())
        { 
            new EditFrmController(new EditFrm(), new EditFrmModel(),view).control();
        }
        if (ae.getSource() == view.getViewMenu())
        { 
            new TransactionsTableController(new TransactionsTable(), new TransactionsTableModel(view.customerId), view).control();
        }
        if (ae.getSource() == view.getPrintMenu())
        { 
            new PrintModel().printHistory(view.customerId);
        }
    }
    public void control(){
        view.getExitMenu().addActionListener(this);
        view.getPrintMenu().addActionListener(this);
        view.getViewMenu().addActionListener(this);
        view.getDeleteMenu().addActionListener(this);
        view.getEditMenu().addActionListener(this);
        view.getBackMenu().addActionListener(this);
        view.getLoanS().addActionListener(this);
        view.getLoginBtn().addActionListener(this);
        view.getSignInBtn().addActionListener(this);
        view.getInfoBtn().addActionListener(this);
    }
    
}
