/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Customer;
import Models.LoginFrmModel;
import Views.LoginFrm;
import Views.LoginView;
import Views.MainPageView;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Mac-Noble
 */
public class LogInController implements ActionListener {
    LoginView view;
    LoginFrmModel model;
    MainPageView main;
    public LogInController(LoginView r, LoginFrmModel m, MainPageView pg) {
        view = r;
        view.getContentPane().setBackground(new Color(14,52,65));
        model = m;
        main = pg;
        view.setLocationRelativeTo(main);
        view.getCancelBtn().addActionListener(this);
        view.getLoginBtn().addActionListener(this);
        view.setVisible(true);
    }
    public void actionPerformed(ActionEvent ae) {            
        if (ae.getSource() == view.getCancelBtn())
        {      
            view.setVisible(false);
            view.dispose();
        }
        if (ae.getSource() == view.getLoginBtn())
        {               
            String _username = view.getUserName().getText();
            String _passWord = new String(view.getPassword().getPassword());
            if(model.logIn(_username, _passWord))
            {
                JOptionPane.showMessageDialog(null, "Login succesful");
                main.getCustomer().setVisible(true);
                main.getServices().setVisible(true);
                main.getLoginBtn().setVisible(false);
                main.getSignInBtn().setVisible(false);
                main.getLab().setText(model.getCustomerName() + "! Welcome"  + " to CrediFi's Main Portal!");
                main.customerId = model.getCustomerID();
                view.dispose();
            }else
                JOptionPane.showMessageDialog(null, "Login unssuccesful");
        }
    }
}
